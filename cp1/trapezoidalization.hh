/**
 * Trapezoidalization:
 *
 * Solution for Question 2:
 * Perform Trapezoidalization of the simple polygon you obtained.
 * Store the information in the required data structures
 *
 * Basic Idea:
 *    sweep, and collect intersection points
 */

#include <CGAL/Cartesian.h>
#include <CGAL/MP_Float.h>
#include <CGAL/Quotient.h>
#include <CGAL/Arr_segment_traits_2.h>
#include <CGAL/Sweep_line_2_algorithms.h>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Polygon_2.h>
#include <list>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Delaunay_triangulation_2.h>
#include <CGAL/Triangulation_vertex_base_with_info_2.h>
#ifdef CGAL_USE_GMP
// GMP is installed. Use the GMP rational number-type.
#include <CGAL/Gmpq.h>
typedef CGAL::Gmpq                                    Number_type;
#else
// GMP is not installed. Use CGAL's exact rational number-type.
#include <CGAL/MP_Float.h>
#include <CGAL/Quotient.h>
typedef CGAL::Quotient<CGAL::MP_Float>                Number_type;
#endif
#include <CGAL/Cartesian.h>

/**
 #include <CGAL/Cartesian.h>
 #include <CGAL/MP_Float.h>
 #include <CGAL/Quotient.h>
 #include <CGAL/Arr_segment_traits_2.h>
 #include <CGAL/Sweep_line_2_algorithms.h>
 #include <list>
 typedef CGAL::Quotient<CGAL::MP_Float>                  NT;
 typedef CGAL::Cartesian<NT>                             Kernel;
 typedef Kernel::Point_2                                 Point_2;
 typedef CGAL::Arr_segment_traits_2<Kernel>              Traits_2;
 typedef Traits_2::Curve_2                               Segment_2;
 **/

//typedef CGAL::Quotient<CGAL::MP_Float>                  NT;
//typedef CGAL::Cartesian<NT>                             Kernel;
//typedef Kernel::Point_2                                 Point_2;
typedef CGAL::Cartesian<Number_type>                                  Coloring_Kernel;
typedef Coloring_Kernel::Point_2                                      Coloring_Point_2;
typedef Coloring_Kernel::Segment_2                                    Segment_2;
//typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
//typedef K::Point_2 Point;
//typedef CGAL::Polygon_2<K> Polygon_2;

using namespace std;

void sortVerticesVertically (Point* points, int l)
{
    // selectionSort
    for (int i = 0 ; i < l - 1 ; i++)
    {
        int p = i;
        for (int j = i + 1 ; j < l ; j++)
        {
            if (points[j].y() < points[p].y() ||
                (points[j].y() == points[p].y() &&
                 points[j].x() < points[p].x()))
            {
                p = j;
            }
        }
        Point point = points[p];
        points[p] = points[i];
        points[i] = point;
    }
}

Segment_2* getSegments (Polygon_2& polygon, int l)
{
    Segment_2 * segments = new Segment_2 [l+1];
    for(int i=0;i<l;i++){
        segments[i] = Segment_2(Coloring_Point_2(polygon.edge(i).source().x(),polygon.edge(i).source().y()),Coloring_Point_2(polygon.edge(i).target().x(),polygon.edge(i).target().y() )  );
        cout<<"edge of polygon -> "<<segments[i]<<" \n";
    }
    //segments[l]=NULL;
    return segments;
}

int getTrapezoidation (Polygon_2& polygon)
{
    Point* vertices = getVertices(polygon);
    int size = polygon.size();
    cout << endl << "Question 2: Perform Trapezoidalization of the simple polygon " << endl << endl;
    sortVerticesVertically(vertices, size);
    
    Segment_2* segments = getSegments(polygon, size);
	Coloring_Point_2 trpzdtn[size][3];
    cout<<" size "<<size<<"\n";
    //    list<Coloring_Point_2> intersections;
    //    CGAL::compute_intersection_points (segments, segments + size , std::back_inserter(intersections));
    //    int temp_intersect = intersections.size();
    //
    //    //segments[size] = Segment_2(Coloring_Point_2 (DBL_MIN,0), Coloring_Point_2 (DBL_MAX , 0 ) );
    //    std::copy (intersections.begin(), intersections.end(),
    //               std::ostream_iterator<Coloring_Point_2 >(std::cout, " asdasd \n \n \n"));
    
    
    
    
    //cout<<"intersections size " <<intersections.size()<<"\n";
    cout<<"Trapezoidation :- \n";
    for (int i = 0 ; i < size ; i++)
    {
        int start = 0 ;
        list<Coloring_Point_2> intersections;//cout<<" Debug \n ";
        list<Coloring_Point_2> intersections_back ;
        list<Coloring_Point_2> :: iterator it ;
        
        segments[size] = Segment_2(Coloring_Point_2 (DBL_MIN,vertices[i].y()), Coloring_Point_2 (DBL_MAX , vertices[i].y() ) );
        
        CGAL::compute_intersection_points (segments, segments + size+1, std::back_inserter(intersections));
        //
        //         for(auto i = intersections.begin();i != intersections.end();i++){
        //             cout<<(*i)<<"FOOFOFOF\n";
        //         }
        cout<<"y coordinate : "<< vertices[i].y()<<" no of intersections :- "<< intersections.size() <<"\n";
        //cout<<"idgdg"<<" " << (*intersections.begin()).x()<<"\n";
        if(intersections.size() % 2 == 0 )
        {
            it = intersections.begin();
            if((*intersections.begin()).x() == vertices[i].x() ){
                cout<<(*it).x()<<":"<< (*it).y();
		trpzdtn[i][0] = *it;                
		it++;
                cout<<"->"<<(*it).x()<<":"<<(*it).y()<<endl;
		trpzdtn[i][1] = *it;		           
		trpzdtn[i][2] = Coloring_Point_2 (DBL_MIN,DBL_MIN);
		}
            
            else
            {
                it = intersections.end();
                it--;
                it--;
		trpzdtn[i][0] = *it;  
                cout<<(*it).x()<<":"<< (*it).y();
                it++;
                trpzdtn[i][1] = *it;  
		cout <<"->"<<(*it).x()<<":"<<(*it).y()<<endl;
                trpzdtn[i][2] = Coloring_Point_2 (DBL_MIN,DBL_MIN);
            }
        }
        else
        {
            it = intersections.begin();
            int pos = 0;
            while((*it).x()!=vertices[i].x())
            {
                it++;
                pos++;
            }
            //it = intersections.begin();
            //it=it+i-1;
            if( (pos)%2 ==1 )
            {    it--;
		trpzdtn[i][0] = *it; 
                cout<<(*it).x()<<":"<< (*it).y()<<"->";
                it++;
		trpzdtn[i][1] = *it; 
                cout<<(*it).x()<<":"<<(*it).y()<<"->";
                it++;
		trpzdtn[i][2] = *it; 
                cout<<(*it).x()<<":"<<(*it).y()<<endl;
            }
        }
    }
    
    return 0;
}
