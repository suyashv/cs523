/**
 * coloring:
 *  solution for Question 5
 */

#include <CGAL/basic.h>

#ifdef CGAL_USE_GMP
  // GMP is installed. Use the GMP rational number-type.
  #include <CGAL/Gmpq.h>
  typedef CGAL::Gmpq                                    Number_type;
#else
  // GMP is not installed. Use CGAL's exact rational number-type.
  #include <CGAL/MP_Float.h>
  #include <CGAL/Quotient.h>
  typedef CGAL::Quotient<CGAL::MP_Float>                Number_type;
#endif


#include <CGAL/Cartesian.h>
#include <CGAL/Arr_segment_traits_2.h>
#include <CGAL/Arr_extended_dcel.h>
#include <CGAL/Arrangement_2.h>
#include <CGAL/graph_traits_Dual_Arrangement_2.h>
#include <CGAL/Arr_face_index_map.h>
#include <climits>
#include <boost/graph/breadth_first_search.hpp>
#include <boost/graph/visitors.hpp>


/**
 * arr_print
 */

//-----------------------------------------------------------------------------
// Print all neighboring vertices to a given arrangement vertex.
//
template<class Arrangement>
void print_neighboring_vertices (typename Arrangement::Vertex_const_handle v)
{
  if (v->is_isolated())
  {
    std::cout << "The vertex (" << v->point() << ") is isolated" << std::endl;
    return;
  }
  typename Arrangement::Halfedge_around_vertex_const_circulator  first, curr;
  typename Arrangement::Vertex_const_handle                      u;
  std::cout << "The neighbors of the vertex (" << v->point() << ") are:";
  first = curr = v->incident_halfedges();
  do
  {
    // Note that the current halfedge is (u -> v):
    u = curr->source();
    std::cout << " (" << u->point() << ")";
    ++curr;
  } while (curr != first);
  std::cout << std::endl;
  return;
}
//-----------------------------------------------------------------------------
// Print all vertices (points) and edges (curves) along a connected component
// boundary.
//
template<class Arrangement>
void print_ccb (typename Arrangement::Ccb_halfedge_const_circulator circ)
{
  typename Arrangement::Ccb_halfedge_const_circulator  curr = circ;
  typename Arrangement::Halfedge_const_handle          he;
  std::cout << "(" << curr->source()->point() << ")";
  do
  {
    he = curr;
    std::cout << "   [" << he->curve() << "]   "
              << "(" << he->target()->point() << ")";
    ++curr;
  } while (curr != circ);
  std::cout << std::endl;
  return;
}
//-----------------------------------------------------------------------------
// Print the boundary description of an arrangement face.
//
template<class Arrangement>
void print_face (typename Arrangement::Face_const_handle f)
{
  // Print the outer boundary.
  if (f->is_unbounded())
  {
    std::cout << "Unbounded face. " << std::endl;
  }
  else
  {
    std::cout << "Outer boundary: ";
    print_ccb<Arrangement> (f->outer_ccb());
  }
  // Print the boundary of each of the holes.
  typename Arrangement::Hole_const_iterator  hole;
  int                                         index = 1;
  for (hole = f->holes_begin(); hole != f->holes_end(); ++hole, ++index)
  {
    std::cout << "    Hole #" << index << ": ";
    print_ccb<Arrangement> (*hole);
  }
  // Print the isolated vertices.
  typename Arrangement::Isolated_vertex_const_iterator  iv;
  for (iv = f->isolated_vertices_begin(), index = 1;
       iv != f->isolated_vertices_end(); ++iv, ++index)
  {
    std::cout << "    Isolated vertex #" << index << ": "
              << "(" << iv->point() << ")" << std::endl;
  }
  return;
}
//-----------------------------------------------------------------------------
// Print the given arrangement.
//
template<class Arrangement>
void print_arrangement (const Arrangement& arr)
{
  CGAL_precondition (arr.is_valid());
  // Print the arrangement vertices.
  typename Arrangement::Vertex_const_iterator  vit;
  std::cout << arr.number_of_vertices() << " vertices:" << std::endl;
  for (vit = arr.vertices_begin(); vit != arr.vertices_end(); ++vit)
  {
    std::cout << "(" << vit->point() << ")";
    if (vit->is_isolated())
      std::cout << " - Isolated." << std::endl;
    else
      std::cout << " - degree " << vit->degree() << std::endl;
  }
  // Print the arrangement edges.
  typename Arrangement::Edge_const_iterator    eit;
  std::cout << arr.number_of_edges() << " edges:" << std::endl;
  for (eit = arr.edges_begin(); eit != arr.edges_end(); ++eit)
    std::cout << "[" << eit->curve() << "]" << std::endl;
  // Print the arrangement faces.
  typename Arrangement::Face_const_iterator    fit;
  std::cout << arr.number_of_faces() << " faces:" << std::endl;
  for (fit = arr.faces_begin(); fit != arr.faces_end(); ++fit)
    print_face<Arrangement> (fit);
  return;
}

/**
 * main
 */


// A property map that reads/writes the information to/from the extended
// face.
template <typename Arrangement, class Type>
class Extended_face_property_map {
public:
  typedef typename Arrangement::Face_handle       Face_handle;
  // Boost property type definitions.
  typedef boost::read_write_property_map_tag      category;
  typedef Type                                    value_type;
  typedef value_type&                             reference;
  typedef Face_handle                             key_type;
  // The get function is required by the property map concept.
  friend reference get(const Extended_face_property_map&, key_type key)
  { return key->data(); }
  // The put function is required by the property map concept.
  friend void put(const Extended_face_property_map&,
                 key_type key, value_type val)
{ key->set_data(val); }
};
typedef CGAL::Cartesian<Number_type>                         Coloring_Kernel;
typedef CGAL::Arr_segment_traits_2<Coloring_Kernel>                   Traits_2;
typedef CGAL::Arr_face_extended_dcel<Traits_2, unsigned int> Dcel;
typedef CGAL::Arrangement_2<Traits_2, Dcel>                  Ex_arrangement;
typedef CGAL::Dual<Ex_arrangement>                           Dual_arrangement;
typedef CGAL::Arr_face_index_map<Ex_arrangement>             Face_index_map;
typedef Extended_face_property_map<Ex_arrangement,unsigned int>
                                                            Face_property_map;
typedef Coloring_Kernel::Point_2                                      Coloring_Point_2;
typedef Coloring_Kernel::Segment_2                                    Segment_2;

int printDualColoring (Polygon_2& polygon) {
  cout << endl << "Question 5: 3-coloring of a graph:" << endl << endl;

  Ex_arrangement  arr;

  // generate arr from polygon

  for (auto i = polygon.edges_begin() ; i != polygon.edges_end() ; i++) {
    Coloring_Point_2 point1 = Coloring_Point_2(i->point(0).x(), i->point(0).y()),
                     point2 = Coloring_Point_2(i->point(1).x(), i->point(1).y());
    insert(arr, Segment_2(point1, point2));
  }

  // Create a mapping of the arrangement faces to indices.
  Face_index_map  index_map(arr);
  // Perform breadth-first search from the unbounded face, using the event
  // visitor to associate each arrangement face with its discover time.
  unsigned int    time = 0;
  boost::breadth_first_search(Dual_arrangement(arr), arr.unbounded_face(),
                              boost::vertex_index_map(index_map).visitor
                              (boost::make_bfs_visitor
                               (stamp_times(Face_property_map(), time,
                                            boost::on_discover_vertex()))));
  // Print the discover time of each arrangement face.
  Ex_arrangement::Face_iterator  fit;
  for (fit = arr.faces_begin(); fit != arr.faces_end(); ++fit) {
    std::cout << "Discover time " << fit->data() << " for ";
    if (fit != arr.unbounded_face()) {
      std::cout << "face ";
      print_ccb<Ex_arrangement>(fit->outer_ccb());
    }
    else std::cout << "the unbounded face." << std::endl;
  }
  return 0;
}

int printDualColoring (Polygon_2& polygon,list<Segment_2>& l) {
  cout << endl << "Question 5: 3-coloring of a graph:" << endl << endl;

  Ex_arrangement  arr;

  // generate arr from polygon

  for (auto i =l.begin() ; i != l.end() ; i++) {
    Coloring_Point_2 point1 = i->vertex(0),
                     point2 = i->vertex(1);
    insert(arr, Segment_2(point1, point2));
  }

  // Create a mapping of the arrangement faces to indices.
  Face_index_map  index_map(arr);
  // Perform breadth-first search from the unbounded face, using the event
  // visitor to associate each arrangement face with its discover time.
  unsigned int    time = 0;
  boost::breadth_first_search(Dual_arrangement(arr), arr.unbounded_face(),
                              boost::vertex_index_map(index_map).visitor
                              (boost::make_bfs_visitor
                               (stamp_times(Face_property_map(), time,
                                            boost::on_discover_vertex()))));
  // Print the discover time of each arrangement face.
  Ex_arrangement::Face_iterator  fit;
  for (fit = arr.faces_begin(); fit != arr.faces_end(); ++fit) {
    std::cout << "Discover time " << fit->data() << " for ";
    if (fit != arr.unbounded_face()) {
      std::cout << "face ";
      print_ccb<Ex_arrangement>(fit->outer_ccb());
    }
    else std::cout << "the unbounded face." << std::endl;
  }
  return 0;
}

