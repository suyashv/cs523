/**
 * main:
 *
 * run all solutions
 */

#include "polygon.hh"
 #include "trapezoidalization.hh"
#include "monotone.hh"
#include "triangulation.hh"
#include "coloring.hh"
#include "art-gallery.hh"

using namespace std;

int main () {
  int choice = 1;
  cout << "Enter 1 to enter points, 2 to read from data.txt:" << endl;
  cin >> choice;

  Polygon_2* polygon = NULL;

  switch (choice) {
    case 1:
      polygon = getPolygonFromInput();
      break;
    case 2:
      polygon = getPolygonFromFile();
      break;
    default:
      cout << "wrong choice ! Enter correct choice please ..." << endl;
  }

  // #1
  displayPolygon(*polygon);

  // #2
   getTrapezoidation(*polygon);

  // #3
  printMonotonePartitions(*polygon);

  // #4
  //printTriangulation(*polygon);
  list<Segment_2> l;
  printTriangulation(*polygon,l);
  // #5
  printDualColoring(*polygon,l);

  find_number_of_guards(*polygon);

  delete polygon;
  return 0;
}
