# CP-1

## Instructions for Ubuntu

### dependencies

```
[sudo] apt-get install libboost-dev libcgal-dev cmake
```

The solution requires __C++11 compatible compiler__. Works on g++4.8.2 on Ubuntu 14.04.

### Build

```
cmake .
cmake .
make
```
