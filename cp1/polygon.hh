/**
 * Polygon:
 *
 * Solution for Question 1:
 *    Create a polygon from a given set of random points
 *
 * Basic Idea:
 *    traverse through the given points, and try to create a polygon
 */

#include <iostream>
#include <fstream>
#include <math.h>
#include <float.h>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Polygon_2.h>


typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef K::Point_2 Point;
typedef CGAL::Polygon_2<K> Polygon_2;

using namespace std;

#define EPSILON std::numeric_limits<double>::epsilon();
Point reference;

void sortVertices(const Point&,Point *,int );
void bottomMostPoint(const Point * vertices,int size,int *);

Point* getVertices (Polygon_2& polygon) {
  int index = 0,
      size = polygon.size();

  Point * vertices = new Point[size];

  for (auto i = polygon.vertices_begin(); i != polygon.vertices_end() ; i++) {
    Point point = *i;
    vertices[index++] = point;
  }

  return vertices;
}


/**
 * print relevant data about the created polygon
 */
void displayPolygon (Polygon_2& polygon) {

  if (polygon.is_simple()) {
    cout << "Simple Polygon" << endl;
  }
  else {
    cout << "Not a Simple Polygon" << endl;
  }

  if (polygon.is_convex()) {
    cout << "Convex Polygon" << endl;
  }
  else {
    cout << "Not a Convex Polygon" << endl;
  }

  cout << "Polygon Points:" << endl;

  int size = polygon.size();
  printf("Vertices :%d\n", size);
  Point* vertices = getVertices(polygon);

  //sortVertices(*polygon.bottom_vertex(),vertices,size);
  for(int i=0;i<size;i++){
    cout << "[ " << vertices[i].x() << ", " << vertices[i].y() << " ]" << endl;
  }

  delete [] vertices;
}

bool areEqual (double i,double j) {
  return fabs(i-j)<= EPSILON;
}


bool comp (const Point& i,const Point& j) {
  double slope1,slope2;
  //Check if on of the points is a reference point.
  if(reference == i){
    return false;
  }
  else if(reference == j){
    return true;
  }
  if(areEqual(i.x(),reference.x())){
    slope1 = DBL_MAX;
  }else{
    slope1 = (i.y() - reference.y())/(i.x()-reference.x());
  }

  if(areEqual(j.x(),reference.x())){
    slope2 = DBL_MAX;
  }else{
    slope2 = (j.y() - reference.y())/(j.x()-reference.x());
  }

  slope1 = atan(slope1);
  slope2 = atan(slope2);

  if(areEqual(slope1,slope2)){
    return i<j;
  }
  return (slope1<slope2);
}

void sortVertices (const Point& ref,Point* vertexList,int size) {
  reference = ref;
  sort(vertexList,vertexList+size,comp);
}

void bottomMostPoint(const Point * vertices,int size,int * index){
  *index = 0;
  for(int i=0;i<size;i++){
    if(vertices[i]<vertices[*index]){
      *index = i;
    }
  }
}

/**
 * get polygon with user input
 */

Polygon_2* getPolygonFromInput () {
  cout << endl << "Question 1: Get Polygon from User input" << endl << endl;
  cout << "Enter n:" << endl;
  int n = 0;
  cin >> n;

  Point* points = new Point[n];

  for (int i = 0 ; i < n ; i++) {
    int x = 0, y = 0;
    cout << "Enter x" << endl;
    cin >> x;
    cout << "Enter y" << endl;
    cin >> y;

    points[i] = Point(x, y);
  }

  int refIndex =0;
  bottomMostPoint(points,n,&refIndex);
  Point ref = points[refIndex];
  sortVertices(ref,points,n);
  Polygon_2* polygon = new Polygon_2(points, points + n);

  return polygon;
}

/**
 * get polygon from file
 */

 Polygon_2* getPolygonFromFile () {
  string filename = "data.txt";
  cout << endl << "Question 1: Get Polygon from given file(" << filename << ")" << endl << endl;
  ifstream file(filename);

  int n = 0;
  file >> n;

  Point* points = new Point[n];

  for (int i = 0 ; i < n ; i++) {
    int x = 0, y = 0;
    file >> x;
    file >> y;

    points[i] = Point(x, y);
  }

  int refIndex =0;
  bottomMostPoint(points,n,&refIndex);
  Point ref = points[refIndex];
  sortVertices(ref,points,n);
  Polygon_2* polygon = new Polygon_2(points, points + n);

  return polygon;
}
