#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Constrained_Delaunay_triangulation_2.h>
#include <CGAL/Triangulation_vertex_base_with_info_2.h>
#include <CGAL/Triangulation_face_base_with_info_2.h>
#include <CGAL/Polygon_2.h>
#include <vector>
#include <iostream>

/*oi domes stis opoies apothikevode oi plirofories gia ta faces kai ta vertices*/

struct FaceInfo2
{
    FaceInfo2(){}
    int nesting_level;
    bool done;

    bool in_domain(){
        return nesting_level%2 == 1;
    }
    bool is_done(){
        return done;
    }
};

struct VertexInfo{

    VertexInfo(){}
    int color; // 1 = R, 2 = G, 3 = B
    bool colored; 
    bool can_change;
    int the_id;

    int get_color(){
        return color;
    }
    bool get_colored(){
        return colored;
    }
    bool get_can_change(){
        return can_change;
    }
    int get_the_id(){
        return the_id;
    }
};

typedef CGAL::Exact_predicates_inexact_constructions_kernel       K;
typedef CGAL::Triangulation_vertex_base_with_info_2<VertexInfo,K> Vbb;
typedef CGAL::Triangulation_face_base_with_info_2<FaceInfo2,K>    Fbb;
typedef CGAL::Constrained_triangulation_face_base_2<K,Fbb>        Fb;
typedef CGAL::Triangulation_data_structure_2<Vbb,Fb>               TDS;
typedef CGAL::Exact_predicates_tag                                Itag;
typedef CGAL::Constrained_Delaunay_triangulation_2<K, TDS, Itag>  CDT;
typedef CDT::Point                                                Point;
typedef CGAL::Polygon_2<K>                                        Polygon_2;
typedef CDT::Face_iterator Face_iterator;

void mark_domains(CDT& ct,
             CDT::Face_handle start,
             int index,
             std::list<CDT::Edge>& border )
{
    if(start->info().nesting_level != -1){
        return;
    }
    std::list<CDT::Face_handle> queue;
    queue.push_back(start);

    while(! queue.empty()){
        CDT::Face_handle fh = queue.front();
        queue.pop_front();
        if(fh->info().nesting_level == -1){
            fh->info().nesting_level = index;
            for(int i = 0; i < 3; i++){
                CDT::Edge e(fh,i);
                CDT::Face_handle n = fh->neighbor(i);
                if(n->info().nesting_level == -1){
                    if(ct.is_constrained(e)) border.push_back(e);
                    else queue.push_back(n);
                }
            }
        }
    }
}


void mark_domains(CDT& cdt)
{
    for(CDT::All_faces_iterator it = cdt.all_faces_begin(); it != cdt.all_faces_end(); ++it){
        it->info().nesting_level = -1;
    }

    int index = 0;
    std::list<CDT::Edge> border;
    mark_domains(cdt, cdt.infinite_face(), index++, border);
    while(! border.empty()){
        CDT::Edge e = border.front();
        border.pop_front();
        CDT::Face_handle n = e.first->neighbor(e.second);
        if(n->info().nesting_level == -1){
            mark_domains(cdt, n, e.first->info().nesting_level+1, border);
        }
    }
}

void insert_polygon(CDT& cdt,const Polygon_2& polygon){
    if ( polygon.is_empty() ) return;
    CDT::Vertex_handle v_prev=cdt.insert(*CGAL::cpp0x::prev(polygon.vertices_end()));
    for (Polygon_2::Vertex_iterator vit=polygon.vertices_begin();
         vit!=polygon.vertices_end();++vit)
    {
        CDT::Vertex_handle vh=cdt.insert(*vit);
        cdt.insert_constraint(vh,v_prev);
        v_prev=vh;
    }
}

bool is_inside(int x, std::vector<int> kati){
    int count = 0;
    for (int i = 0; i <= kati.size(); i++) {
        if(x == kati[i]){
            count++;
            break;
        }
    }

    if(count > 0){
        return true;
    }else{
    return false;
    }
}

int find_number_of_guards(Polygon_2& polygon1){
    CDT cdt;
    insert_polygon(cdt,polygon1);
    mark_domains(cdt);

    std::cout<<std::endl<<"3-Colors R(1),G(2) and B(3)"<<std::endl<<std::endl;

    int the_id = 1;
     int facelets_count = 0;
    
    for (CDT::Finite_faces_iterator fit=cdt.finite_faces_begin();
         fit!=cdt.finite_faces_end();++fit)
    {
        if ( fit->info().in_domain() ){
            fit->info().done = false;
            fit->vertex(0)->info().colored = false;
            fit->vertex(1)->info().colored = false;
            fit->vertex(2)->info().colored = false;

            fit->vertex(0)->info().can_change = true;
            fit->vertex(1)->info().can_change = true;
            fit->vertex(2)->info().can_change = true;


            fit->vertex(0)->info().the_id = the_id;
            the_id++;
            fit->vertex(1)->info().the_id = the_id;
            the_id++;
            fit->vertex(2)->info().the_id = the_id;
            the_id++;

             facelets_count++;
        }

    }


    int count=0;

    std::vector<int> red;
    std::vector<int> green;
    std::vector<int> blue;
    for (int k=0; k<=facelets_count; k++) {


    for (CDT::Finite_faces_iterator fit=cdt.finite_faces_begin();
         fit!=cdt.finite_faces_end();++fit)
    {
        if ( fit->info().in_domain() ){


            if(count == 0){

                fit->vertex(0)->info().color = 1;
                red.push_back(fit->vertex(0)->info().the_id);
                fit->vertex(1)->info().color = 2;
                green.push_back(fit->vertex(1)->info().the_id);
                fit->vertex(2)->info().color = 3;
                blue.push_back(fit->vertex(2)->info().the_id);

                fit->vertex(0)->info().colored = true;
                fit->vertex(1)->info().colored = true;
                fit->vertex(2)->info().colored = true;
                fit->info().done = true;

            }else{

                if(fit->info().done == false  ){

                    if (is_inside(fit->vertex(0)->info().the_id, green) == true) {

                    if (is_inside(fit->vertex(1)->info().the_id, red) == true) {

                        if (is_inside(fit->vertex(2)->info().the_id, blue) == false) {
                             fit->vertex(2)->info().color = 3;
                             blue.push_back(fit->vertex(2)->info().the_id);
                             fit->info().done = true;
                            continue;

                        }else{
                             fit->info().done = true;
                            continue;
                        }


                    }else if(is_inside(fit->vertex(1)->info().the_id, blue) == true){

                        if (is_inside(fit->vertex(2)->info().the_id, red) == false) {
                            fit->vertex(2)->info().color = 1;
                            red.push_back(fit->vertex(2)->info().the_id);
                            fit->info().done = true;
                            continue;

                        }else{
                            fit->info().done = true;
                            continue;
                        }
                    }else if(is_inside(fit->vertex(2)->info().the_id, red) == true){

                        if (is_inside(fit->vertex(1)->info().the_id, blue) == false) {
                            fit->vertex(1)->info().color = 3;
                        blue.push_back(fit->vertex(1)->info().the_id);
                            fit->info().done = true;
                            continue;
                        }else{
                            fit->info().done = true;
                            continue;
                        }
                    }else if(is_inside(fit->vertex(2)->info().the_id, blue) == true){

                        if (is_inside(fit->vertex(1)->info().the_id, red) == false) {
                            fit->vertex(1)->info().color = 1;
                            red.push_back(fit->vertex(1)->info().the_id);
                            fit->info().done = true;
                            continue;
                        }else{
                            fit->info().done = true;
                            continue;
                        }
                    }
                    }
                        if (is_inside(fit->vertex(0)->info().the_id, red) == true) { 
                        if (is_inside(fit->vertex(1)->info().the_id, green) == true) {
                            if (is_inside(fit->vertex(2)->info().the_id, blue) == false) {
                                fit->vertex(2)->info().color = 3;
                                blue.push_back(fit->vertex(2)->info().the_id);
                                fit->info().done = true;
                                continue;
                            }else{
                                fit->info().done = true;
                                continue;
                            }
                        }else if(is_inside(fit->vertex(1)->info().the_id, blue) == true){
                            if (is_inside(fit->vertex(2)->info().the_id, green) == false) {
                                fit->vertex(2)->info().color = 2;
                                green.push_back(fit->vertex(2)->info().the_id);
                                fit->info().done = true;
                                continue;
                            }else{
                                fit->info().done = true;
                                continue;
                            }
                        }else if(is_inside(fit->vertex(2)->info().the_id, green) == true){
                            if (is_inside(fit->vertex(1)->info().the_id, blue) == false) {
                                fit->vertex(1)->info().color = 3;
                                blue.push_back(fit->vertex(1)->info().the_id);
                                fit->info().done = true;
                                continue;
                            }else{
                                fit->info().done = true;
                                continue;
                            }
                        }else if(is_inside(fit->vertex(2)->info().the_id, blue) == true){
                            if (is_inside(fit->vertex(1)->info().the_id, green) == false) {
                                fit->vertex(1)->info().color = 2;
                               green.push_back(fit->vertex(1)->info().the_id);
                                fit->info().done = true;
                                continue;

                            }else{
                                fit->info().done = true;
                                continue;
                            }
                        }
                    }//
                    if (is_inside(fit->vertex(0)->info().the_id, blue) == true) { 
                        if (is_inside(fit->vertex(1)->info().the_id, red) == true) {
                            if (is_inside(fit->vertex(2)->info().the_id, green) == false) {
                                fit->vertex(2)->info().color = 2;
                                green.push_back(fit->vertex(2)->info().the_id);
                                fit->info().done = true;
                                continue;
                            }else{
                                fit->info().done = true;
                                continue;
                            }
                        }else if(is_inside(fit->vertex(1)->info().the_id, green) == true){

                            if (is_inside(fit->vertex(2)->info().the_id, red) == false) {
                                fit->vertex(2)->info().color = 1;
                                red.push_back(fit->vertex(2)->info().the_id);
                                fit->info().done = true;
                                continue;
                            }else{
                                fit->info().done = true;
                                continue;
                            }
                        }else if(is_inside(fit->vertex(2)->info().the_id, red) == true){
                            if (is_inside(fit->vertex(1)->info().the_id, green) == false) {
                                fit->vertex(1)->info().color = 2;
                                green.push_back(fit->vertex(1)->info().the_id);
                                fit->info().done = true;
                                continue;

                            }else{
                                fit->info().done = true;
                                continue;
                            }
                        }else if(is_inside(fit->vertex(2)->info().the_id, green) == true){
                            if (is_inside(fit->vertex(1)->info().the_id, red) == false) {
                                fit->vertex(1)->info().color = 1;
                                red.push_back(fit->vertex(1)->info().the_id);
                                fit->info().done = true;
                                continue;
                            }else{
                                fit->info().done = true;
                                continue;
                            }
                        }
                    } else if(is_inside(fit->vertex(1)->info().the_id, green) == true){ 
                          if (is_inside(fit->vertex(0)->info().the_id, red) == true) {
                              if (is_inside(fit->vertex(2)->info().the_id, blue) == false) {
                                  fit->vertex(2)->info().color = 3;
                                  blue.push_back(fit->vertex(2)->info().the_id);
                                  fit->info().done = true;
                                  continue;
                              }else{
                                  fit->info().done = true;
                                  continue;
                              }
                          } else if (is_inside(fit->vertex(0)->info().the_id,blue) == true) {

                              if (is_inside(fit->vertex(2)->info().the_id, red) == false) {
                                  fit->vertex(2)->info().color = 1;
                                  red.push_back(fit->vertex(2)->info().the_id);
                                  fit->info().done = true;
                                  continue;

                              }else{
                                  fit->info().done = true;
                                  continue;
                              }
                          }  else if (is_inside(fit->vertex(2)->info().the_id, red) == true) {

                              if (is_inside(fit->vertex(0)->info().the_id, blue) == false) {
                                  fit->vertex(0)->info().color = 3;
                                  blue.push_back(fit->vertex(0)->info().the_id);
                                  fit->info().done = true;
                                  continue;

                              }else{
                                  fit->info().done = true;
                                  continue;
                              }
                          } else if (is_inside(fit->vertex(2)->info().the_id, blue) == true) {

                              if (is_inside(fit->vertex(0)->info().the_id, red) == false) {
                                  fit->vertex(0)->info().color = 1;
                                  red.push_back(fit->vertex(0)->info().the_id);
                                  fit->info().done = true;
                                  continue;

                              }else{
                                  fit->info().done = true;
                                  continue;
                              }
                          }
                    }  else if(is_inside(fit->vertex(1)->info().the_id, red) == true){ 

                        if (is_inside(fit->vertex(0)->info().the_id, green) == true) {

                            if (is_inside(fit->vertex(2)->info().the_id, blue) == false) {
                                fit->vertex(2)->info().color = 3;
                                blue.push_back(fit->vertex(2)->info().the_id);
                                fit->info().done = true;
                                continue;

                            }else{
                                fit->info().done = true;
                                continue;
                            }
                        } else if (is_inside(fit->vertex(0)->info().the_id, blue) == true) {

                            if (is_inside(fit->vertex(2)->info().the_id, green) == false) {
                                fit->vertex(2)->info().color = 2;
                                green.push_back(fit->vertex(2)->info().the_id);
                                fit->info().done = true;
                                continue;
                            }else{
                                fit->info().done = true;
                                continue;
                            }
                        }  else if (is_inside(fit->vertex(2)->info().the_id, green) == true) {

                            if (is_inside(fit->vertex(0)->info().the_id, blue) == false) {
                                fit->vertex(0)->info().color = 3;
                                blue.push_back(fit->vertex(0)->info().the_id);
                                fit->info().done = true;
                                continue;

                            }else{
                                fit->info().done = true;
                                continue;
                            }
                        } else if (is_inside(fit->vertex(2)->info().the_id, blue) == true) {

                            if (is_inside(fit->vertex(0)->info().the_id, green) == false) {
                                fit->vertex(0)->info().color = 2;
                                green.push_back(fit->vertex(0)->info().the_id);
                                fit->info().done = true;
                                continue;

                            }else{
                                fit->info().done = true;
                                continue;
                            }

                        }
                    }
                   if(is_inside(fit->vertex(1)->info().the_id, blue) == true){ 

                       if (is_inside(fit->vertex(0)->info().the_id, red) == true) {

                           if (is_inside(fit->vertex(2)->info().the_id, green) == false) {
                               fit->vertex(2)->info().color = 2;
                               green.push_back(fit->vertex(2)->info().the_id);
                               fit->info().done = true;
                               continue;

                           }else{
                               fit->info().done = true;
                               continue;
                           }



                       } else if (is_inside(fit->vertex(0)->info().the_id, green) == true) {

                           if (is_inside(fit->vertex(2)->info().the_id, red) == false) {
                               fit->vertex(2)->info().color = 1;
                               red.push_back(fit->vertex(2)->info().the_id);
                               fit->info().done = true;
                               continue;

                           }else{
                               fit->info().done = true;
                               continue;
                           }

                       }  else if (is_inside(fit->vertex(2)->info().the_id, red) == true) {

                           if (is_inside(fit->vertex(0)->info().the_id, green) == false) {
                               fit->vertex(0)->info().color = 2;
                               green.push_back(fit->vertex(0)->info().the_id);
                               fit->info().done = true;
                               continue;

                           }else{
                               fit->info().done = true;
                               continue;
                           }

                       } else if (is_inside(fit->vertex(2)->info().the_id, green) == true) {

                           if (is_inside(fit->vertex(0)->info().the_id, red) == false) {
                               fit->vertex(0)->info().color = 1;
                               red.push_back(fit->vertex(0)->info().the_id);
                               fit->info().done = true;
                               continue;

                           }else{
                               fit->info().done = true;
                               continue;
                           }

                       }
                   } 
                    if(is_inside(fit->vertex(2)->info().the_id, green) == true){ // G
                        if (is_inside(fit->vertex(1)->info().the_id, red) == true) {

                            if (is_inside(fit->vertex(0)->info().the_id, blue) == false) {
                                fit->vertex(0)->info().color = 3;
                                blue.push_back(fit->vertex(0)->info().the_id);
                                fit->info().done = true;
                                continue;

                            }else{
                                fit->info().done = true;
                                continue;
                            }
                        } else if (is_inside(fit->vertex(1)->info().the_id, blue) == true) {

                            if (is_inside(fit->vertex(0)->info().the_id, red) == false) {
                                fit->vertex(0)->info().color = 1;
                                red.push_back(fit->vertex(0)->info().the_id);
                                fit->info().done = true;
                                continue;

                            }else{
                                fit->info().done = true;
                                continue;
                            }
                        }  else if (is_inside(fit->vertex(0)->info().the_id, red) == true) {

                            if (is_inside(fit->vertex(1)->info().the_id,blue) == false) {
                                fit->vertex(1)->info().color = 3;
                                blue.push_back(fit->vertex(1)->info().the_id);
                                fit->info().done = true;
                                continue;

                            }else{
                                fit->info().done = true;
                                continue;
                            }
                        } else if (is_inside(fit->vertex(0)->info().the_id, blue) == true) {

                            if (is_inside(fit->vertex(1)->info().the_id, red) == false) {
                                fit->vertex(1)->info().color = 1;
                                red.push_back(fit->vertex(1)->info().the_id);
                                fit->info().done = true;
                                continue;

                            }else{
                                fit->info().done = true;
                                continue;
                            }
                        }
                            if(is_inside(fit->vertex(2)->info().the_id, red) == true){ //R

                            if (is_inside(fit->vertex(1)->info().the_id, green) == true) {

                                if (is_inside(fit->vertex(0)->info().the_id, blue) == false) {
                                    fit->vertex(0)->info().color = 3;
                                    blue.push_back(fit->vertex(0)->info().the_id);
                                    fit->info().done = true;
                                    continue;

                                }else{
                                    fit->info().done = true;
                                    continue;
                                }
                            } else if (is_inside(fit->vertex(1)->info().the_id, blue) == true) {

                                if (is_inside(fit->vertex(0)->info().the_id, green) == false) {
                                    fit->vertex(0)->info().color = 2;
                                    green.push_back(fit->vertex(0)->info().the_id);
                                    fit->info().done = true;
                                    continue;

                                }else{
                                    fit->info().done = true;
                                    continue;
                                }
                            }  else if (is_inside(fit->vertex(0)->info().the_id, green) == true) {

                                if (is_inside(fit->vertex(1)->info().the_id,blue) == false) {
                                    fit->vertex(1)->info().color = 3;
                                    blue.push_back(fit->vertex(1)->info().the_id);
                                    fit->info().done = true;
                                    continue;

                                }else{
                                    fit->info().done = true;
                                    continue;
                                }
                            } else if (is_inside(fit->vertex(0)->info().the_id, blue) == true) {

                                if (is_inside(fit->vertex(1)->info().the_id, green) == false) {
                                    fit->vertex(1)->info().color = 2;
                                    green.push_back(fit->vertex(1)->info().the_id);
                                    fit->info().done = true;
                                    continue;

                                }else{
                                    fit->info().done = true;
                                    continue;
                                }
                            }
                        }
                    }
                    if(is_inside(fit->vertex(2)->info().the_id, blue) == true){ // B
                        if (is_inside(fit->vertex(1)->info().the_id, red) == true) {

                            if (is_inside(fit->vertex(0)->info().the_id, green) == false) {
                                fit->vertex(0)->info().color = 2;
                                green.push_back(fit->vertex(0)->info().the_id);
                                fit->info().done = true;
                                continue;

                            }else{
                                fit->info().done = true;
                                continue;
                            }

                        } else if (is_inside(fit->vertex(1)->info().the_id, green) == true) {

                            if (is_inside(fit->vertex(0)->info().the_id, red) == false) {
                                fit->vertex(0)->info().color = 1;
                                red.push_back(fit->vertex(0)->info().the_id);
                                fit->info().done = true;
                                continue;

                            }else{
                                fit->info().done = true;
                                continue;
                            }

                        }  else if (is_inside(fit->vertex(0)->info().the_id, red) == true) {

                            if (is_inside(fit->vertex(1)->info().the_id,green) == false) {
                                fit->vertex(1)->info().color = 2;
                                green.push_back(fit->vertex(1)->info().the_id);
                                fit->info().done = true;
                                continue;

                            }else{
                                fit->info().done = true;
                                continue;
                            }
                        } else if (is_inside(fit->vertex(0)->info().the_id, green) == true) {

                            if (is_inside(fit->vertex(1)->info().the_id, red) == false) {
                                fit->vertex(1)->info().color = 1;
                                red.push_back(fit->vertex(1)->info().the_id);
                                fit->info().done = true;
                                continue;

                            }else{
                                fit->info().done = true;
                                continue;
                            }
                        }
                    }
                }
            }
         count++;
        }
    }
    }
    int loopdex =0;

    for (CDT::Finite_faces_iterator fit=cdt.finite_faces_begin();
         fit!=cdt.finite_faces_end();++fit)
    {
        if ( fit->info().in_domain() ){
            std::cout << fit->vertex(0)->point() << " color: " << fit->vertex(0)->info().get_color() << std::endl;
            std::cout << fit->vertex(1)->point() << " color: " << fit->vertex(1)->info().get_color() << std::endl;
            std::cout << fit->vertex(2)->point() << " color: " << fit->vertex(2)->info().get_color() << std::endl << std::endl;
            loopdex++;
        }
    }

    int see1 = red.size();
    int see2 = green.size();
    int see3 = blue.size();


    int min = see1;
    int selected = 1;

    if(see2 < min){
        min = see2;
        int selected = 2;
    }
    if(see3 < min){
        min = see3;
        int selected = 3;
    }
    
    std::cout << std::endl;
    std::cout << "No. of vertices having same color"<< std::endl;
    std::cout << "R " << see1 << std::endl;
    std::cout << "G " << see2 << std::endl;
    std::cout << "B " << see3 << std::endl;
    std::cout << std::endl;

    if(selected == 1){
        std::cout << "Place guards at R\n" << std::endl;


    }else if(selected == 2){

         std::cout << "Place guards at G\n" << std::endl;

    }else{
         std::cout << "Place guards at B\n" << std::endl;
    }
    std::cout << "Guards at the vertices given below : " << std::endl;
    if(selected == 1){
        for (int i = 0; i < red.size(); i++) {
                bool get_me_out = false;
            for (CDT::Finite_faces_iterator fit=cdt.finite_faces_begin();
                 fit!=cdt.finite_faces_end();++fit)
            {
                if ( fit->info().in_domain() ){

                    for (int k=0; k < 3; k++) {

                        if (red.at(i) == fit->vertex(k)->info().get_the_id() ) {
                            std::cout << fit->vertex(k)->point() << std::endl;
                            get_me_out = true;
                        }
                    }
                }
                if(get_me_out == true){
                    break;
                }
            }
        }
    }else if(selected == 2){
        for (int i = 0; i< green.size(); i++) {
             bool get_me_out = false;
            for (CDT::Finite_faces_iterator fit=cdt.finite_faces_begin();
                 fit!=cdt.finite_faces_end();++fit)
            {
                if ( fit->info().in_domain() ){
                    for (int k=0; k < 3; k++) {
                        if (green.at(i) == fit->vertex(k)->info().get_the_id() ) {
                            std::cout << fit->vertex(k)->point() << std::endl;
                           get_me_out = true;
                        }
                    }
                }
                if(get_me_out == true){
                    break;
                }
            }
        }
    }else if(selected == 3){
        for (int i = 0; i< blue.size(); i++) {
              bool get_me_out = false;
            for (CDT::Finite_faces_iterator fit=cdt.finite_faces_begin();
                 fit!=cdt.finite_faces_end();++fit)
            {
                if ( fit->info().in_domain() ){
                    for (int k=0; k < 3; k++) {
                        if (blue.at(i) == fit->vertex(k)->info().get_the_id() ) {
                            std::cout << fit->vertex(k)->point() << std::endl;
                            get_me_out = true;
                        }
                    }
                }
                if(get_me_out == true){
                    break;
                }
            }
        }
    }
  

      return 0;
}