/**
 * Monotone:
 *
 * Solution for Question 3:
 *    Obtain Monotone Polygons from the simple polygon
 */

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Partition_traits_2.h>
#include <CGAL/partition_2.h>
#include <CGAL/point_generators_2.h>
#include <CGAL/random_polygon_2.h>
#include <cassert>
#include <list>

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Partition_traits_2<K>                         Traits;
typedef Traits::Point_2                                     Point_2;
typedef Traits::Polygon_2                                   Polygon_2_Trait;
typedef std::list<Polygon_2_Trait>                          Polygon_list;
typedef CGAL::Creator_uniform_2<int, Point_2>               Creator;
typedef CGAL::Random_points_in_square_2<Point_2, Creator>   Point_generator;

using namespace std;

void getPolygonTrait (Polygon_2& polygon, Polygon_2_Trait& poly) {
  for (auto i = polygon.vertices_begin() ; i != polygon.vertices_end() ; i++) {
    auto point = *i;
    poly.push_back(Point_2(point.x(), point.y()));
  }
}

void printMonotonePartitions (Polygon_2& poly) {
  Polygon_2_Trait polygon;
  getPolygonTrait(poly, polygon);

  Polygon_list monotone_partitions;
  CGAL::y_monotone_partition_2(polygon.vertices_begin(),
                               polygon.vertices_end(),
                               std::back_inserter(monotone_partitions));

  cout << endl << "Question 3: Monotone Partitions:" << endl << endl;
  int index = 0;
  for (auto i = monotone_partitions.begin() ; i != monotone_partitions.end() ; i++) {
    cout << "Polygon#" << index++ << ": " << *i << endl;
  }
}
