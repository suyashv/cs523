/**
 * Triangulation:
 *
 * Solution for Question 4:
 *    For each monotone polygon, perform triangulation.
 *    Store the information in the required data structures
 */

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Delaunay_triangulation_2.h>
#include <CGAL/Triangulation_vertex_base_with_info_2.h>
#ifdef CGAL_USE_GMP
  // GMP is installed. Use the GMP rational number-type.
  #include <CGAL/Gmpq.h>
  typedef CGAL::Gmpq                                    Number_type;
#else
  // GMP is not installed. Use CGAL's exact rational number-type.
  #include <CGAL/MP_Float.h>
  #include <CGAL/Quotient.h>
  typedef CGAL::Quotient<CGAL::MP_Float>                Number_type;
#endif
#include <CGAL/Cartesian.h>

typedef CGAL::Exact_predicates_inexact_constructions_kernel Kernel;
typedef CGAL::Triangulation_vertex_base_with_info_2<unsigned int, Kernel> Vb;
typedef CGAL::Triangulation_data_structure_2<Vb> Tds;
typedef CGAL::Delaunay_triangulation_2<Kernel, Tds> Delaunay;
typedef CGAL::Cartesian<Number_type>                         Coloring_Kernel;
typedef Coloring_Kernel::Point_2                                      Coloring_Point_2;
typedef Coloring_Kernel::Segment_2                                    Segment_2;

using namespace std;

void printTriangulation (Polygon_2& polygon) {
  Point* points = getVertices(polygon);

  Delaunay triangulation;
  list< pair<Point,unsigned> > vec;

  for (int i = 0 ; i < polygon.size() ; i++) {
    vec.push_back(make_pair(points[i], i));
  }

  triangulation.insert(vec.begin(), vec.end());

  int index = 1;
  cout << endl << "Question 4: Triangulation:" << endl << endl;
  for(auto face = triangulation.finite_faces_begin() ; face != triangulation.finite_faces_end() ; face++) {
    cout << "Triangle #" << index++ << ":\t" << triangulation.triangle(face) << endl;
    cout << "Vertex 0 index:\t" << face->vertex(0)->info() << endl;
    cout << "Vertex 1 index:\t" << face->vertex(1)->info() << endl;
    cout << "Vertex 2 index:\t" << face->vertex(2)->info() << endl;
  }
}

void printTriangulation (Polygon_2& polygon,list<Segment_2>& l) {
  Point* points = getVertices(polygon);

  Delaunay triangulation;
  list< pair<Point,unsigned> > vec;

  for (int i = 0 ; i < polygon.size() ; i++) {
    vec.push_back(make_pair(points[i], i));
  }

  triangulation.insert(vec.begin(), vec.end());

  int index = 1;
  cout << endl << "Question 4: Triangulation:" << endl << endl;
  for(auto face = triangulation.finite_faces_begin() ; face != triangulation.finite_faces_end() ; face++) {
    cout << "Triangle #" << index++ << ":\t" << triangulation.triangle(face) << endl;
      int index1 = face->vertex(0)->info();
      int index2 = face->vertex(1)->info();
      int index3 = face->vertex(2)->info();
    cout << "Vertex 0 index:\t" << face->vertex(0)->info() << endl;
    cout << "Vertex 1 index:\t" << face->vertex(1)->info() << endl;
    cout << "Vertex 2 index:\t" << face->vertex(2)->info() << endl;
      
    l.push_back(Segment_2(Coloring_Point_2(points[index1].x(),points[index1].y()),Coloring_Point_2(points[index2].x(),points[index2].y())));
l.push_back(Segment_2(Coloring_Point_2(points[index2].x(),points[index2].y()),Coloring_Point_2(points[index3].x(),points[index3].y())));
      l.push_back(Segment_2(Coloring_Point_2(points[index3].x(),points[index3].y()),Coloring_Point_2(points[index1].x(),points[index1].y())));
  }
}
